# escape=`

FROM mcr.microsoft.com/powershell:lts-windowsservercore-ltsc2022

SHELL ["powershell", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

USER ContainerAdministrator

# We have to enable TLS1.2 to download from GitHub.
RUN New-Item -ItemType directory -Path C:\Downloads; `
    [Net.ServicePointManager]::SecurityProtocol = [Net.ServicePointManager]::SecurityProtocol -bor [Net.SecurityProtocolType]::Tls12; `
    Invoke-Webrequest "https://github.com/git-for-windows/git/releases/download/v2.34.1.windows.1/MinGit-2.34.1-64-bit.zip" -OutFile 'C:\Downloads\git.zip' -UseBasicParsing; `
    Invoke-Webrequest "https://github.com/git-lfs/git-lfs/releases/download/v3.0.2/git-lfs-windows-amd64-v3.0.2.zip" -OutFile 'C:\Downloads\git-lfs.zip' -UseBasicParsing; `
    Invoke-Webrequest "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-helper/gitlab-runner-helper.x86_64-windows.exe" -OutFile 'C:\Downloads\gitlab-runner-helper.exe' -UseBasicParsing;
    
RUN Expand-Archive -Path 'C:\Downloads\git.zip' 'C:\Program Files\git\'; `
    Expand-Archive -Path 'C:\Downloads\git-lfs.zip' 'C:\Program Files\git-lfs\'; `
    New-Item -ItemType directory -Path 'C:\Program Files\gitlab-runner-helper\'; `
    mv 'C:\Downloads\gitlab-runner-helper.exe' 'C:\Program Files\gitlab-runner-helper\';

RUN pwsh --version
    
SHELL ["pwsh", "-Command", "$ErrorActionPreference = 'Stop'; $ProgressPreference = 'SilentlyContinue';"]

# We enable `GIT_CONFIG_NOSYSTEM` and unset `include.path` in order to avoid recursive includes in the Git configuration
# This can be removed once we move the MinGit installation out of the `C:\Program Files\git` directory. See
# https://gitlab.com/gitlab-org/gitlab/-/issues/239013#note_401347751
RUN [Environment]::SetEnvironmentVariable('Path', $env:Path + ';C:\Program Files\git\cmd;C:\Program Files\git-lfs;C:\Program Files\gitlab-runner-helper', [EnvironmentVariableTarget]::Machine); `
    $env:Path = [System.Environment]::GetEnvironmentVariable('Path', 'Machine'); `
    $env:GIT_CONFIG_NOSYSTEM=1; `
    & $env:ProgramFiles\git\cmd\git.exe config --system --unset-all include.path; `
    & $env:ProgramFiles\git\cmd\git.exe config --system core.longpaths true; `
    & $env:ProgramFiles\git\cmd\git.exe config --system core.symlinks false; `
    & $env:ProgramFiles\git\cmd\git.exe lfs install --skip-repo

COPY ["entrypoint.cmd", ".\\"]

ENTRYPOINT ["entrypoint.cmd"]

