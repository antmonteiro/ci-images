#!/bin/bash

set -e

#
# Determine an appropriate parallelism setting (autotools builds are usually slow enough as it is)
# The jobs formula matches with what ninja seems to think is a good default on my machine, and is fairly conservative anyway.
#
cores="$(nproc --all)"
jobs="$(($cores * 5 / 4))"

NAME="$1"
GIT_URL="$2"
GIT_BRANCH_OR_TAG_REF="${3:-master}"
WORKDIR="$(pwd)/src/$NAME"

if [ -z "$NAME" ]
then
    echo "No name specified (usage: $(basename "$0") <name> <url> <optional tag/branch>)" >&2
    exit 1
elif [ -z "$GIT_URL" ]
then
    echo "No git url specified (usage: $(basename "$0") <name> <url> <optional tag/branch>)" >&2
    exit 1
else
    echo "Will attempt to build '$NAME' ($GIT_BRANCH_OR_TAG_REF) from $GIT_URL"
fi

if [ -e "$WORKDIR" ]
then
    echo "There appears to be something else already at '$WORKDIR'" >&2
    echo "This may mess up your build of '$NAME', proceeding to remove it with prejudice..." >&2
    rm -rf "$WORKDIR"
fi

generate_autotools_script ()
{
    local host="$1"
    local prefix="$2"

    cat << SCRIPT
    ./autogen.sh && ./configure --prefix="$prefix" --host="$host" $AUTOTOOLS_EXTRA && make -j$jobs && make install
SCRIPT
}

dump_build_info ()
{
    local arch="$1"
    local host="$2"
    local prefix="$3"

    cat << INFO
== autotools build

Will attempt to build '$NAME' ($GIT_BRANCH_OR_TAG_REF) from $GIT_URL

 - Android arch       : $arch
 - Host arch triplet  : $host
 - Destination prefix : $prefix

INFO
}

#
# Wrapper around the standard autotools invocation that cleans up after itself.
# In-source autotools builds are quite messy and we have to run N times after
# each other for N arches. In particular we want to avoid the situation that make
# thinks a full-recompile is not necessary because of something that was only valid
# for the previous arch being built.
#
run_autotools_build ()
{
    local arch="$1"
    local host="$2"
    local prefix="$3"

    # generate logs in the CI to more easily 'see' where one arch starts and another ends
    dump_build_info "$arch" "$host" "$prefix"

    # enable command tracing to better follow what the script is doing from CI logs
    # use a subshell () to avoid noise from the post-build logging & clean up
    (
        set -x
        git clone --depth 1 --branch "$GIT_BRANCH_OR_TAG_REF" "$GIT_URL" "$WORKDIR"
        cd "$WORKDIR"
        STANDALONE_EXTRA="--arch=$arch $STANDALONE_EXTRA" /opt/helpers/build-standalone "$(generate_autotools_script "$host" "$prefix")"
    )

    echo "Build of $NAME succeeded, cleaning up $WORKDIR ..."

    # the promised clean up: deliberately not very clever, in the hope that it will be reliable.
    rm -rf "$WORKDIR"
}

# do the actual build for each supported Android arch, compiler host triplet, prefix
if [[ ! $ONLY_ARM64 ]]; then
    run_autotools_build arm arm-linux-androideabi /opt/kdeandroid-arm
fi
if [[ ! $ONLY_ARM32 ]]; then
    run_autotools_build arm64 aarch64-linux-android /opt/kdeandroid-arm64
fi
